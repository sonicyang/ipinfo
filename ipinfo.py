#!/usr/bin/env python3
# -----------------------------------------------------------------------------
# Name:        ipinfo.py
# Purpose:
#
# Author:      Sonic
#
# Created:     26/04/2016
# Version:     0.01 collect ip infomation from whatismyipaddress.com or others
#              0.02 support return a dictionary with the standard column
#              0.03 disable Insecure Request Warning
#              0.04 support auto detect local host public IP
#              0.05 add the commnent for host parameter and
#                   using "" for local machine IP inquery instead of None
#              0.06 add retry function and handle request timeout exception
#                   properly
#              0.07 support python3
# -----------------------------------------------------------------------------

import argparse
import requests
import os
import sys
import urllib3
try:
    from requests.packages.urllib3.exceptions import InsecureRequestWarning
    urllib3.disable_warnings(InsecureRequestWarning)
except ImportError:
    pass
# settings for the query web site
query_url = 'https://ipinfo.io/'
detect_url = query_url
ME_STR = 'me'
chrome_ua = ""
std_columns = (u'Hostname', u'IP', u'ISP', u'ASN', u'Country', u'Organization')


def getArg():
    _retry = 0
    parser = argparse.ArgumentParser()
    parser.add_argument('host', help='Input an IP address or FQDN. '
                        'default will find the public IP using on the device',
                        nargs='?',
                        default=ME_STR)
    parser.add_argument('-u', '--url', default=query_url,
                        help='Default setting is {}'.format(query_url))
    parser.add_argument('-H', '--httpHeaders',  default=chrome_ua)
    parser.add_argument('-r', '--raw',  default=True, action='store_true',
                        help='Return a dictionary or print a table out')
    parser.add_argument('-R', '--retry', type=int, default=_retry,
                        help='Set retry times, default is {}'.format(_retry))
    parser.add_argument('-w', '--wait', type=int, default=10,
                        help='Set the wait time for http get, '
                             'retry wait will be an half of it')
    return parser.parse_args()


def httpGet(headers, host, url='', wait=15):
    r = None
    output_temp = '{:=^40}'
    target = '%s%s' % (url, host)
    try:
        r = requests.get(
                target,
                headers=headers,
                timeout=wait,
                verify=False).text
    except KeyboardInterrupt:
        s = 'Stop http get process by user'
        print(output_temp.format(s))
        raise
    except requests.exceptions.Timeout:
        # print(output_temp.format(e))
        raise
    return r

def my_ip(headers='', url=detect_url, wait=5):
    ip = httpGet(headers, '', url, wait)
    return ip

def show_data(data, raw=False, key_attr='tr', _sep=':', columns=std_columns):
    _temp = {}
    output = '{}{}{}'
    soup = BeautifulSoup(data, 'html.parser')
    data_lst = soup.find_all(key_attr)
    for d in data_lst:
        _temp[d.findChild().get_text().replace(':', '')] =\
                        d.findChild().findNext().get_text()
    std_dict = {k: _temp.get(k, None) for k in columns}
    if raw:
        return std_dict
    else:
        pad = [output.format(
            c,
            ':'+' '*(20 - len(c)),
            std_dict[c]) for c in columns]
        print(os.linesep.join(pad))


if __name__ == '__main__':
    html = None
    arg = getArg()
    if arg.host.strip() == ME_STR:
        arg.host = my_ip(arg.httpHeaders)
    retry_times = list(range(arg.retry+1))
    retry_times.reverse()
    for rt in retry_times:
        try:
            html = httpGet(arg.httpHeaders, arg.host,
                           url=arg.url, wait=arg.wait)
        except KeyboardInterrupt:
            sys.exit(1)
        except requests.exceptions.Timeout:
            if rt > 0:
                print('HTTP response timeout. '
                      'Waiting for {} second to try again'.format(arg.wait/2))
    if html:
        if arg.raw:
            print(html)
        else:
            show_data(html)
            print('-' * 30)
    else:
        print('Cannot find any thing for {} from {}'.format(arg.host, arg.url))
