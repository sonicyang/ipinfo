### Purpose
Get the IP Geo-Location and ISP infomation 

### Mini Howto
```
❯ ./ipinfo.py 151.101.64.81
{
  "ip": "151.101.64.81",
  "anycast": true,
  "city": "San Francisco",
  "region": "California",
  "country": "US",
  "loc": "37.7621,-122.3971",
  "org": "AS54113 Fastly, Inc.",
  "postal": "94107",
  "timezone": "America/Los_Angeles",
  "readme": "https://ipinfo.io/missingauth"
}

# Find the local machine's public IP address information
$./ipinfo.py
```

### Packages
requests